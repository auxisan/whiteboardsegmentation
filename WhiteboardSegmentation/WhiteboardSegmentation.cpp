// WhiteboardSegmentation.cpp : Defines the entry point for the console application.

#include "stdafx.h"

#include "opencv2\core\core.hpp"
#include "opencv2\highgui\highgui.hpp"
#include "opencv2\imgproc\imgproc.hpp"
#include "opencv2\opencv.hpp"
#include <iostream>
#include <vector>
#include <queue>

using namespace cv;
using namespace std;

// direction vectors
const int dx[] = {+1, 0, -1, 0};
const int dy[] = {0, +1, 0, -1};

std::pair<int, int> current_pixel;
std::queue<pair <int,int >> pixel_queue;
Mat imageMat = cv::imread("orangeman.jpg", CV_LOAD_IMAGE_COLOR);
Mat labelMat (imageMat.size(), CV_8UC1, UCHAR_MAX);

bool pixel_similarity(int a, int b, int direction)
{ 
	cv::Vec3b BGR = imageMat.at<Vec3b>(a,b);
	cv::Vec3b BGR_neighbor = imageMat.at<Vec3b>(a + dx[direction], b + dy[direction]);
	return (((BGR[0] - BGR_neighbor[0]) + (BGR[1] - BGR_neighbor[1]) + (BGR[2] - BGR_neighbor[2])) < 20); //check later
}

bool check_label(int s,int t)
{
	if (labelMat.at<uchar>(s,t) == UCHAR_MAX)
		return false;
	else 
		return true;
} 

void grow_region(pair<int, int>current_pixel)
{  
	int x = get<0>(current_pixel);
	int y = get<1>(current_pixel);
	for (int direction = 0; direction < 4; ++direction)
	{ 
	 	if ((x + dx[direction]) < 0 || (x + dx[direction]) == imageMat.rows) // out of bounds
	 		continue;
		if ((y + dy[direction]) < 0 || (y + dy[direction]) == imageMat.cols) // out of bounds
			continue;

		//check if neighbor pixel is labeled
	 	bool is_labeled = check_label(x + dx[direction], y + dy[direction]);
		if (is_labeled == true)
	  		continue;
		 
		// check if neighbor pixel is similar
		bool is_similar = pixel_similarity(x, y, direction);
	 	if (is_similar == true)
	 	{ 
	 		 labelMat.at<uchar>(x + dx[direction], y + dy[direction]) = labelMat.at<uchar>(x,y); //label with current label
	 		// put in queue
	 		pixel_queue.push(make_pair(x + dx[direction], y + dy[direction]));
		}
		 
	} 
}



int main()
{
	int top_label = 1;
	   
    if (imageMat.empty())
    {
            std::cerr << "ERROR: Could not read image " << std::endl;
            return -1;
    }
	
	cv::namedWindow("input", CV_WINDOW_NORMAL);
    cv::namedWindow("labeled", CV_WINDOW_NORMAL);

	for (int i=0;i<labelMat.rows;i++)
	{
		for (int j = 0;j<labelMat.cols;j++)
		{ 

			// label new pixels
			if(labelMat.at<uchar>(i,j) ==  UCHAR_MAX) 
			{
				pixel_queue.push(make_pair(i,j)); //add pixel to queue
				labelMat.at<uchar>(i,j) = top_label++;
			}

			// pop pixel off queue
			while(!pixel_queue.empty())
			{
				current_pixel = pixel_queue.front();
				pixel_queue.pop();
				grow_region(current_pixel);
				
			}
			
		}
	}
	
	imshow("input", imageMat);
	imshow("labeled", labelMat);
	return 0;
}

